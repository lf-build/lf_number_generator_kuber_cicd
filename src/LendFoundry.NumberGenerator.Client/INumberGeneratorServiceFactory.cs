﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.NumberGenerator.Client
{
    public interface INumberGeneratorServiceFactory
    {
        IGeneratorService Create(ITokenReader reader);
    }
}
