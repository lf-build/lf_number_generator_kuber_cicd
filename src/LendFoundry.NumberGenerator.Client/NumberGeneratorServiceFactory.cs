﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.NumberGenerator.Client
{
    public class NumberGeneratorServiceFactory : INumberGeneratorServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public NumberGeneratorServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }
        public NumberGeneratorServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }

        private Uri Uri { get; }

        public IGeneratorService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("number_generator");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new NumberGeneratorService(client);
        }
       
    }
}
