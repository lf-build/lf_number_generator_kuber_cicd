﻿using LendFoundry.Foundation.Client;

using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator.Client
{
    public class NumberGeneratorService : IGeneratorService
    {
        public NumberGeneratorService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

	    public async Task<string> TakeNext(string entityId)
	    {
			var request = new RestRequest("/{entityid}", Method.POST);
		    request.AddUrlSegment("entityid", entityId);
            var response = await Client.ExecuteAsync<NextNumber>(request);
            return response.Number;
	    }

        public async Task<string> TakeNext(string entityId, List<string> subEntityIds)
        {
            var request = new RestRequest("/{entityid}/{subEntityId}", Method.POST);
            request.AddUrlSegment("entityid", entityId);
            AppendSubEntityIds(request,subEntityIds);
            var response = await Client.ExecuteAsync<NextNumber>(request);
            return response.Number;
        }

        private static void AppendSubEntityIds(IRestRequest request, IReadOnlyList<string> subEntityIds)
        {
            if (subEntityIds == null || !subEntityIds.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < subEntityIds.Count; index++)
            {
                var tag = subEntityIds[index];
                url = url + $"/{{tag{index}}}";
                request.AddUrlSegment($"tag{index}", tag);
            }
            request.Resource = url;
        }

    }
}
