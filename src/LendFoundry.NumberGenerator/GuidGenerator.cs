﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public class GuidGenerator : IGenerator
    {
        public GuidGenerator
        (
            INumberGeneratorRepository repository,
            ConfigType config,
            ITenantTime tenantTime
        )
        {
            Repository = repository;
            Config = config;
            TenantTime = tenantTime;
        }

        private INumberGeneratorRepository Repository { get; }

        private ConfigType Config { get; }

        private ITenantTime TenantTime { get; }

        public async Task<string> GenerateIt(string entityId)
        {
            return await Task.Run(() =>
            {
                var number = Guid.NewGuid().ToString("N");
                var detail = new NumberDetail
                {
                    EntityId = entityId,
                    GeneratedAt = new TimeBucket(TenantTime.Today),
                    GeneratedNumber = $"{Config.Prefix?.Trim()}{Config.Separator?.Trim()}{number}{Config.Separator?.Trim()}{Config.Suffix?.Trim()}",
                    Number = number,
                    Prefix = Config.Prefix,
                    Separator = Config.Separator,
                    Suffix = Config.Suffix,
                    Type = NumberType.Guid
                };

                Repository.Add(detail);

                return detail.GeneratedNumber;
            });
        }

        public async Task<string> GenerateItAsync(string entityId, List<string> subEntityIds)
        {
            return await GenerateIt(entityId + "_" + string.Join("_", subEntityIds));
        }
    }
}
