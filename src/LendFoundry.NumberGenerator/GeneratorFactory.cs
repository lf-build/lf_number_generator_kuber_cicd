﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.NumberGenerator
{
    public class GeneratorFactory : IGeneratorFactory
    {
        public GeneratorFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }
		
	    public IGenerator Create(string entityId)
	    {
			IGenerator generator = null;
			var tenantTime = Provider.GetService<ITenantTime>();
			var repository = Provider.GetService<INumberGeneratorRepository>();
            var numberRepository = Provider.GetService<INumberRepository>();

            var configuration = Provider.GetService<IConfiguration>();

		    ConfigType entityConfiguration;
		    if (configuration==null || configuration.Configurations==null || !configuration.Configurations.ContainsKey(entityId))
			    throw new NotFoundException($"The Entity Key {entityId} it is not found.");

		    configuration.Configurations.TryGetValue(entityId, out entityConfiguration);

		    switch (entityConfiguration.Type)
		    {
				case NumberType.Incremental:
					generator = new IncrementalGenerator(repository, numberRepository, entityConfiguration, tenantTime);
					break;
				case NumberType.Guid:
					generator = new GuidGenerator(repository, entityConfiguration, tenantTime);
					break;
			}

			return generator;
		}
    }
}
