﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public class GeneratorService : IGeneratorService
    {
        public GeneratorService(IGeneratorFactory factory)
        {
            Factory = factory;
        }

        private IGeneratorFactory Factory { get; }

        public async Task<string> TakeNext(string entityId) => await Factory.Create(entityId).GenerateIt(entityId);

        public async Task<string> TakeNext(string entityId, List<string> subEntityId) => await Factory.Create(entityId).GenerateItAsync(entityId,subEntityId);
    }
}
