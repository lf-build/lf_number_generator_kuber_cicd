﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator.Persistence
{
    public class NumberGeneratorRepository : MongoRepository<INumberDetail, NumberDetail>, INumberGeneratorRepository
    {
        static NumberGeneratorRepository()
        {
            BsonClassMap.RegisterClassMap<NumberDetail>(map =>
            {
                map.AutoMap();
                map.MapMember(f => f.Type).SetSerializer(new EnumSerializer<NumberType>(BsonType.String));

                var type = typeof(NumberDetail);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public NumberGeneratorRepository(ITenantService tenantService, IMongoConfiguration configuration) :
                base(tenantService, configuration, "number-generator")
        {
            CreateIndexIfNotExists("primary-key", Builders<INumberDetail>.IndexKeys.Ascending(u => u.TenantId).Ascending(u => u.EntityId).Ascending(u => u.GeneratedNumber),true);
        }


        public async Task<IEnumerable<INumberDetail>> GetAll()
        {
            return await Query.ToListAsync();
        }
    }
}
