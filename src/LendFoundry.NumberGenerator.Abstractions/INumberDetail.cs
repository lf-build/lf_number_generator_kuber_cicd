﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.NumberGenerator
{
    public interface INumberDetail : IAggregate
    {
		string EntityId { get; set; }
		TimeBucket GeneratedAt { get; set; }
        string Prefix { get; set; }
        string Number { get; set; }
        string Suffix { get; set; }
        string Separator { get; set; }
        string GeneratedNumber { get; set; }
        NumberType Type { get; set; }
    }
}
