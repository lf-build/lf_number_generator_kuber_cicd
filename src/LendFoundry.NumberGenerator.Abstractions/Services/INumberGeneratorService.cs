﻿namespace LendFoundry.NumberGenerator
{
    public interface INumberGeneratorService
    {
        string Generate();
    }
}
