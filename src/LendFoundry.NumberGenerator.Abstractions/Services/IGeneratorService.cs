﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public interface IGeneratorService
    {
        Task<string> TakeNext(string entityId);
        Task<string> TakeNext(string entityId, List<string> subEntityId);

    }
}
