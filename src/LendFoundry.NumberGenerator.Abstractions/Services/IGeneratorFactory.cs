﻿namespace LendFoundry.NumberGenerator
{
    public interface IGeneratorFactory
    {
	    IGenerator Create(string entityId);
    }
}
