﻿using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public interface INumberRepository
    {
        Task<int> TakeNextNumber(string entityId, int startsWith);
    }
}
