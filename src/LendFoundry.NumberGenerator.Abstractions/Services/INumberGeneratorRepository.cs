﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator
{
    public interface INumberGeneratorRepository : IRepository<INumberDetail>
    {
        Task<IEnumerable<INumberDetail>> GetAll();
    }
}
