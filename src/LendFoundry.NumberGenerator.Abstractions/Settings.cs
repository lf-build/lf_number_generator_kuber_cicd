﻿using System;

namespace LendFoundry.NumberGenerator
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "number-generator";
    }
}
