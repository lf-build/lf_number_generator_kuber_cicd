﻿using System.Collections.Generic;

namespace LendFoundry.NumberGenerator
{
    public class Configuration : IConfiguration
    {
        public IDictionary<string, ConfigType> Configurations { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}
