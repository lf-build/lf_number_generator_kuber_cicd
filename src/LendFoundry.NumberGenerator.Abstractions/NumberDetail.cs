﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.NumberGenerator
{
    public class NumberDetail : Aggregate, INumberDetail
    {
		public string EntityId { get; set; }
        public TimeBucket GeneratedAt { get; set; }
        public string Prefix { get; set; }
        public string Number { get; set; }
        public string Suffix { get; set; }
        public string Separator { get; set; }
        public string GeneratedNumber { get; set; }
        public NumberType Type { get; set; }
    }
}
