﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.NumberGenerator
{
    public interface INumber : IAggregate
    {
        string EntityId { get; set; }
        int LatestNumber { get; set; }
    }
}
