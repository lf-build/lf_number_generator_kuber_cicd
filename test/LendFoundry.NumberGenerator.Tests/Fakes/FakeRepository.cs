﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.NumberGenerator.Tests.Fakes
{
    public class FakeRepository : INumberGeneratorRepository
    {
        public List<INumberDetail> InMemoryData { get; set; } = new List<INumberDetail>();

        public void Add(INumberDetail item)
        {
            item.Id = Guid.NewGuid().ToString("N");
            InMemoryData.Add(item);
        }

        public Task<IEnumerable<INumberDetail>> All(Expression<Func<INumberDetail, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            return Task.Run(() =>
            {
                return InMemoryData
                    .AsQueryable()
                    .Where(query)
                    .AsEnumerable();
            });
        }

        public int Count(Expression<Func<INumberDetail, bool>> query)
        {
            return InMemoryData
                .AsQueryable()
                .Count(query);
        }

        public Task<INumberDetail> Get(string id)
        {
            throw new NotImplementedException();
        }
		
	    public async Task<IEnumerable<INumberDetail>> GetAll()
        {
            return await Task.FromResult(InMemoryData);
        }

        public void Remove(INumberDetail item)
        {
            throw new NotImplementedException();
        }

        public void Update(INumberDetail item)
        {
            throw new NotImplementedException();
        }
    }
}
