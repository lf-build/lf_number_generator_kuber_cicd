﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Tests.Fakes;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.NumberGenerator.Tests
{
    public class NumberGeneratorServiceTest
    {
        [Fact]
        public void IncrementalWhenbasicOperation()
        {
	        var configType = new ConfigType()
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();

            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;
			var listdb = repo.GetAll();

			Assert.Equal($"PRE-1-SUF", number);
		}

		[Fact]
		public void IncrementalWhenNoPrefix()
		{
			var configType = new ConfigType
			{
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();

            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;
			var listdb = repo.GetAll();

			Assert.Equal($"-1-SUF", number);
		}

		[Fact]
		public void IncrementalWhenNoSuffix()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Type = NumberType.Incremental,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);
			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.Equal($"PRE-1-", number);
		}

		[Fact]
		public void IncrementalWhenNoSeparator()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Suffix = "SUF",
				Type = NumberType.Incremental,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);
			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.Equal($"PRE1SUF", number);
		}

		[Fact]
		public void IncrementalWhenNoPrefixSuffixAndSeparator()
		{
			var configType = new ConfigType
			{
				Type = NumberType.Incremental,
			};
			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);
			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.Equal($"1", number);
		}

		[Fact]
		public void IncrementalWhenPaddingLeft()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				LeftPadding = 5,
			};
			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);
			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.Equal($"PRE-00001-SUF", number);
		}

		[Fact]
		public void IncrementalWhenStartsFromInformed()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				StartsFrom = 999
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			config.Configurations.Add("loan", configType);
			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.Equal($"PRE-999-SUF", number);
		}

		[Fact]
		public void IncrementalWhenLongNumber()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				LeftPadding = 100
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;
			var strBuilder = new StringBuilder();
			while (strBuilder.Length < configType.LeftPadding - 1) strBuilder.Append("0");

			Assert.Equal($"PRE-{strBuilder}1-SUF", number);
		}

		[Fact]
		public void IncrementalWhenMultiRequests()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				LeftPadding = 5
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);

			var threadCount = 10;
			Parallel.For(0, threadCount, async el =>
            {
                Task.Delay(el).Wait();
                await service.TakeNext("loan");
            });

			var list = repo.GetAll().Result;
			var duplicatedKeys = list.GroupBy(x => x)
									 .Where(group => group.Count() > 1)
									 .Select(group => group.Key).ToList();

			Assert.Empty(duplicatedKeys);
		}

		[Fact]
		public void HashWhenBasicOperation()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Guid,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.StartsWith("PRE-", number);
			Assert.EndsWith("-SUF", number);
		}

		[Fact]
		public void HashWhenNoPrefix()
		{
			var configType = new ConfigType
			{
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Guid,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.True(!number.StartsWith("PRE-"));
			Assert.EndsWith("-SUF", number);
		}

		[Fact]
		public void HashWhenNoSuffix()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Type = NumberType.Guid,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.StartsWith("PRE-", number);
			Assert.True(!number.EndsWith("-SUF"));
		}

		[Fact]
		public void HashWhenNoSeparator()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Suffix = "SUF",
				Type = NumberType.Guid,
			};

			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.StartsWith("PRE", number);
			Assert.EndsWith("SUF", number);
			Assert.True(!number.Contains("-"));
		}

		[Fact]
		public void HashWhenNoPrefixSuffixAndSeparator()
		{
			var configType = new ConfigType
			{
				Type = NumberType.Guid,
			};
			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);
			var number = service.TakeNext("loan").Result;

			Assert.True(!number.StartsWith("PRE"));
			Assert.True(!number.EndsWith("SUF"));
			Assert.True(!number.Contains("-"));
		}

		[Fact]
		public void HashWhenMultiRequests()
		{
			var configType = new ConfigType
			{
				Prefix = "PRE",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Guid
			};
			var config = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};
			config.Configurations.Add("loan", configType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, config);

			var threadCount = 10;
            List<Task> tasks = new List<Task>();
			Parallel.For(0, threadCount, el =>
			{
				Task.Delay(el).Wait();
                tasks.Add(service.TakeNext("loan"));
			});
            Task.WaitAll(tasks.ToArray());

			var list =  repo.GetAll().Result;
			var duplicatedKeys = list.GroupBy(x => x)
									 .Where(group => group.Count() > 1)
									 .Select(group => group.Key);

			Assert.Empty(duplicatedKeys);
		}

		[Fact]
	    public void HasTwoEntityTypes()
	    {
		    var loanType = new ConfigType
		    {
				Prefix = "LOAN",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				StartsFrom = 0,
				LeftPadding = 3
			};

			var merchantType = new ConfigType
			{
				Prefix = "MER",
				Separator = "-",
				Suffix = "SUF",
				Type = NumberType.Incremental,
				StartsFrom = 100,
				LeftPadding = 3
			};

			var configLoan = new Configuration
			{
				Configurations = new Dictionary<string, ConfigType>()
			};

			configLoan.Configurations.Add("loan", loanType);
			configLoan.Configurations.Add("merchant", merchantType);

			var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var service = BuildService(repo, numberRepository, configLoan);

			var numberLoan =  service.TakeNext("loan").Result;

			service = BuildService(repo, numberRepository, configLoan);
			var numberMerchant = service.TakeNext("merchant").Result;

			Assert.Equal("LOAN-001-SUF", numberLoan);
			Assert.Equal("MER-100-SUF", numberMerchant);
		}

        public IGeneratorService BuildService(INumberGeneratorRepository repository, INumberRepository numberRepository, IConfiguration configuration)
        {
            return new GeneratorService(BuildFactory(repository,numberRepository,configuration));
        }

        public IGeneratorFactory BuildFactory(INumberGeneratorRepository repository, INumberRepository numberRepository, IConfiguration configuration)
        {
            var mock = new Mock<IServiceProvider>();
            mock.Setup(a => a.GetService(typeof(ITenantTime))).Returns(Mock.Of<ITenantTime>());
            mock.Setup(a => a.GetService(typeof(INumberGeneratorRepository))).Returns(repository);
            mock.Setup(a => a.GetService(typeof(INumberRepository))).Returns(numberRepository);
            mock.Setup(a => a.GetService(typeof(IConfiguration))).Returns(configuration);
            return new GeneratorFactory(mock.Object);
        }

        [Fact]
        public void ReturnGuidGenerator()
        {
            var configType = new ConfigType
            {
                Prefix = "PRE",
                Separator = "-",
                Suffix = "SUF",
                Type = NumberType.Guid
            };
            var config = new Configuration
            {
                Configurations = new Dictionary<string, ConfigType>()
            };
            config.Configurations.Add("loan", configType);

            var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var factory = BuildFactory(repo, numberRepository, config);
            Assert.IsType<GuidGenerator>(factory.Create("loan"));
        }

        [Fact]
        public void ReturnIncrementGenerator()
        {
            var configType = new ConfigType
            {
                Prefix = "PRE",
                Separator = "-",
                Suffix = "SUF",
                Type = NumberType.Incremental
            };
            var config = new Configuration
            {
                Configurations = new Dictionary<string, ConfigType>()
            };
            config.Configurations.Add("loan", configType);

            var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var factory = BuildFactory(repo, numberRepository, config);
            Assert.IsType<IncrementalGenerator>(factory.Create("loan"));
        }

        [Fact]
        public void FactoryThrowExceptionOnInvalidEntity()
        {
            var configType = new ConfigType
            {
                Prefix = "PRE",
                Separator = "-",
                Suffix = "SUF",
                Type = NumberType.Incremental
            };
            var config = new Configuration
            {
                Configurations = new Dictionary<string, ConfigType>()
            };
            config.Configurations.Add("loan", configType);

            var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();
            var factory = BuildFactory(repo, numberRepository, config);
            Assert.Throws<NotFoundException>(() => factory.Create("dummy"));
        }

        [Fact]
        public void IncrementalWithSubEntityId()
        {
            var configType = new ConfigType()
            {
                Prefix = "R",
                Separator = "",
                Suffix = "",
                Type = NumberType.Incremental
            };

            var config = new Configuration
            {
                Configurations = new Dictionary<string, ConfigType>()
            };

            config.Configurations.Add("loan", configType);

            var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();

            var service = BuildService(repo, numberRepository, config);
            var number = service.TakeNext("loan",new List<string> { "L00001" }).Result;

            Assert.Equal($"R1", number);

            number = service.TakeNext("loan", new List<string> { "L00001" }).Result;
            Assert.Equal($"R2", number);


            number = service.TakeNext("loan", new List<string> { "L00002" }).Result;

            Assert.Equal($"R1", number);

            number = service.TakeNext("loan", new List<string> { "L00002" }).Result;
            Assert.Equal($"R2", number);

        }

        [Fact]
        public void IncrementalWithSubEntityIdTwoLevel()
        {
            var configType = new ConfigType()
            {
                Prefix = "R",
                Separator = "",
                Suffix = "",
                Type = NumberType.Incremental,
            };
            var subConfigType = new ConfigType()
            {
                Prefix = "L",
                Separator = "",
                Suffix = "",
                Type = NumberType.Incremental
            };
            var config = new Configuration
            {
                Configurations = new Dictionary<string, ConfigType>()
            };

            config.Configurations.Add("loan", configType);
            config.Configurations.Add("subloan", subConfigType);

            var repo = new FakeRepository();
            var numberRepository = new FakeNumberRepository();

            var service = BuildService(repo, numberRepository, config);
            var number = service.TakeNext("loan", new List<string> { "L00001" }).Result;

            Assert.Equal($"R1", number);

            number = service.TakeNext("subloan", new List<string> { "L00001", "R1" }).Result;
            Assert.Equal($"L1", number);

            number = service.TakeNext("subloan", new List<string> { "L00001", "R1" }).Result;
            Assert.Equal($"L2", number);


            number = service.TakeNext("loan", new List<string> { "L00002" }).Result;

            Assert.Equal($"R1", number);

            number = service.TakeNext("subloan", new List<string> { "L00002" , "R2"}).Result;
            Assert.Equal($"L1", number);

            number = service.TakeNext("subloan", new List<string> { "L00002", "R2" }).Result;
            Assert.Equal($"L2", number);

        }

    }
}
