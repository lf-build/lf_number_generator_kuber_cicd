
using LendFoundry.Foundation.Client;
using LendFoundry.NumberGenerator;
using LendFoundry.NumberGenerator.Client;

using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Tenant.Client.Tests
{
    public class NumberGeneratorServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; } = new Mock<IServiceClient>();

        public NumberGeneratorService GetNumberGeneratorService()
        {
            return new NumberGeneratorService(ServiceClient.Object);
        }

        [Fact]
        public void TakeNextReturnNumber()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.ExecuteAsync<NextNumber>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .ReturnsAsync(new NextNumber() { EntityId="loan", Number="1" });

            var numberGeneratorService = GetNumberGeneratorService();
            var response = numberGeneratorService.TakeNext("loan").Result;

            Assert.Equal("1",response);
        }


    }
}
