﻿using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.NumberGenerator.Api.Controllers;
using LendFoundry.Tenant.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Tenant.Api.Tests
{
    public class NumberGeneratorApiControllerTests
    {
        private Mock<IGeneratorService> TenantService { get; } = new Mock<IGeneratorService>();

        private ApiController GetTenantController()
        {
            return new ApiController(TenantService.Object);
        }

        [Fact]
        public void GeneratNumberReturnOk()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.TakeNext("loan")).ReturnsAsync("1");
            var response = controller.GenerateNumber("loan").Result;
            Assert.IsType<OkObjectResult>(response);
            var numberGeneratorObject = ((OkObjectResult)response).Value;
            Assert.IsType<NextNumber>(numberGeneratorObject);
            NextNumber numberResponse = numberGeneratorObject as NextNumber;
            Assert.NotNull(numberResponse);
            Assert.Equal("1", numberResponse.Number);
            Assert.Equal("loan", numberResponse.EntityId);
        }

        [Fact]
        public void GeneratNumberReturn404()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.TakeNext("loan")).Throws(new NotFoundException("entityid not found"));
            var response = controller.GenerateNumber("loan").Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(404, tenantObject.StatusCode);
        }

    }
}
